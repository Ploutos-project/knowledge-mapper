USE treedb;

INSERT INTO trees (id, height, name) VALUES
  ('giantRedwood', 94, 'Giant Redwood'),
  ('silverBirch', 15, 'Silver Birch')
;
